 ## Setup DavFS ##

apt install davfs2
mkdir /srv/Konfiguration/WebDAV
mv /etc/davfs2/secrets /srv/Konfiguration/WebDAV/secrets
ln -s /srv/Konfiguration/WebDAV/secrets /etc/davfs2/secrets
chmod 600 /srv/Konfiguration/WebDAV/secrets
chown root:root /srv/Konfiguration/WebDAV/secrets

touch /srv/Konfiguration/WebDAV/media-webstorage.mount
touch /srv/Konfiguration/WebDAV/media-webstorage.automount
ln -s /srv/Konfiguration/WebDAV/media-webstorage.mount /etc/systemd/system/media-webstorage.mount
ln -s /srv/Konfiguration/WebDAV/media-webstorage.automount /etc/systemd/system/media-webstorage.automount

systemctl daemon-reload
systemctl enable media-webstorage.automount
