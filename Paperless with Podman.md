groupadd -r -g 2000 paperless
useradd -r -u 2000 -g paperless -d /srv/Nutzdaten/Paperless -s /sbin/nologin -c "user for Paperless" paperless

mkdir -p /srv/Nutzdaten/Paperless/{data,media,export}
mkdir -p /media/webstorage/Paperless
chown -R paperless:paperless /srv/Nutzdaten/Paperless
chmod 550 /srv/Nutzdaten/Paperless
chmod 750 /srv/Nutzdaten/Paperless/*

podman container run -d \
--name paperless \
--network host \
-v "/srv/Nutzdaten/Paperless/data:/usr/src/paperless/data" \
-v "/srv/Nutzdaten/Paperless/media:/usr/src/paperless/media" \
-v "/srv/Nutzdaten/Paperless/export:/usr/src/paperless/export" \
-v "/media/webstorage/Paperless:/usr/src/paperless/consume" \
--env-file "/srv/Konfiguration/Paperless/paperless.env" \
ghcr.io/paperless-ngx/paperless-ngx

podman exec -it paperless python3 manage.py createsuperuser
