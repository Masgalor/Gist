 # Cheatsheet for Git

```
# Sign all availabel commits
git filter-branch -f --commit-filter 'git commit-tree -S "$@";' HEAD
```

```
# Change author and committer for all available commits
git filter-branch -f --env-filter "GIT_AUTHOR_NAME='Masgalor'; GIT_AUTHOR_EMAIL='spam@masgalor.de';GIT_COMMITTER_NAME='Masgalor'; GIT_COMMITTER_EMAIL='spam@masgalor.de';" HEAD
```
